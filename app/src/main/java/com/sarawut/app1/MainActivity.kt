package com.sarawut.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar!!.title = "HELLO"
        intentingFullname()
    }

    private fun intentingFullname() {
        val button = findViewById<Button>(R.id.button)

        button.setOnClickListener {
            val fullname: String = findViewById<TextView>(R.id.name).getText().toString()

            val password: String = findViewById<TextView>(R.id.password).getText().toString()

            Log.d("Send",fullname + password)
            val intent = Intent(this, HelloActivity::class.java)
            intent.putExtra("fullname", fullname)

            intent.putExtra("password", password)
            startActivity(intent)
        }
    }

}