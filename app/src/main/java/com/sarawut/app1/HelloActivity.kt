package com.sarawut.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import org.w3c.dom.Text

class HelloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
        supportActionBar!!.title = "HELLO"

        val fullnameIntent: String = intent.getStringExtra("fullname").toString()

        //set on layout
        findViewById<TextView>(R.id.fullname).setText(fullnameIntent)
    }
}